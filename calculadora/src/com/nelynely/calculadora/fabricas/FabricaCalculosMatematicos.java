package com.nelynely.calculadora.fabricas;

import com.nelynely.calculadora.classes.Adicao;
import com.nelynely.calculadora.classes.Divisao;
import com.nelynely.calculadora.classes.Multiplicacao;
import com.nelynely.calculadora.classes.Potenciacao;
import com.nelynely.calculadora.classes.Subtracao;
import com.nelynely.calculadora.interfaces.CalculadorMatematico;

public class FabricaCalculosMatematicos {

	public CalculadorMatematico criarCalculador(int numero1, int numero2, String calculo) {
		if (calculo.equals("+")) {
			return new Adicao(numero1, numero2);
		} else if (calculo.equals("-")) {
			return new Subtracao(numero1, numero2);
		} else if (calculo.equals("*")) {
			return new Multiplicacao(numero1, numero2);
		} else if (calculo.equals("/")) {
			return new Divisao(numero1, numero2);
		} else if (calculo.equals("^")) {
			return new Potenciacao(numero1, numero2);
		} else {
			return null;
		}
	}
}
