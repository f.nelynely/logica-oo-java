package com.nelynely.calculadora.interfaces;

public interface CalculadorMatematico {
	
	Boolean validar();
	int calcular();
}
