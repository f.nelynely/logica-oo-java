package com.nelynely.zoologico.classes;

public interface Animavel {
	
	Boolean ehAdulto();
	void emitirBarulho();
	void morrer();

}
